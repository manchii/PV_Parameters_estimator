/*
 * estimador_app.h
 *
 *  Created on: 14 mar. 2019
 *      Author: Ronny Zárate Ferreto
 */
#include<stdio.h>
#include"xparameters.h"
#include"xgpio.h"
#include"xil_io.h"
#include"xwrapper_fixed_estimator.h"


#define INT2U32(x) *(u32*)&x

